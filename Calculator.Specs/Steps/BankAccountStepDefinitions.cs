namespace Calculator.Specs.Steps;

[Binding]
public class BankAccountStepDefinitions
{
    [Given(@"the account has a balance of (.*)\$")]
    public void GivenTheAccountHasABalanceOf(int balance)
    {
        ScenarioContext.StepIsPending();
    }

    [When(@"the Account Holder requests (.*)\$ to be withdrawn")]
    public void WhenTheAccountHolderRequestsToBeWithdrawn(int amount)
    {
        ScenarioContext.StepIsPending();
    }

    [Then(@"the bank account should dispense (.*)\$")]
    public void ThenTheBankAccountShouldDispense(int amount)
    {
        ScenarioContext.StepIsPending();
    }

    [Then(@"the account balance should be (.*)\$")]
    public void ThenTheAccountBalanceShouldBe(int balance)
    {
        ScenarioContext.StepIsPending();
    }

    [Then(@"the bank should not dispense any money")]
    public void ThenTheBankShouldNotDispenseAnyMoney()
    {
        ScenarioContext.StepIsPending();
    }

    [Then(@"the account balance should remain (.*)\$")]
    public void ThenTheAccountBalanceShouldRemain(int p0)
    {
        ScenarioContext.StepIsPending();
    }

    [Then(@"the Account Holder should be informed that they have insufficient funds")]
    public void ThenTheAccountHolderShouldBeInformedThatTheyHaveInsufficientFunds()
    {
        ScenarioContext.StepIsPending();
    }

    [Given(@"the account is locked")]
    public void GivenTheAccountIsLocked()
    {
        ScenarioContext.StepIsPending();
    }

    [When(@"the Account Holder requests any amount to be withdrawn")]
    public void WhenTheAccountHolderRequestsAnyAmountToBeWithdrawn()
    {
        ScenarioContext.StepIsPending();
    }

    [Then(@"the Account Holder should be informed that their account is locked")]
    public void ThenTheAccountHolderShouldBeInformedThatTheirAccountIsLocked()
    {
        ScenarioContext.StepIsPending();
    }
}