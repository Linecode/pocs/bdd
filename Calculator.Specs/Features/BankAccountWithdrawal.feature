Feature: Bank account withdrawal
    As an Account Holder,
    I want to be able to withdraw money from my bank account,
    So that I can use cash for various transactions.
    
Scenario: Successful withdrawal from a bank account
    Given the account has a balance of 1000$
    When the Account Holder requests 100$ to be withdrawn
    Then the bank account should dispense 100$
    And the account balance should be 900$

Scenario: Unsuccessful withdrawal due to insufficient funds
    Given the account has a balance of 100$
    When the Account Holder requests 200$ to be withdrawn
    Then the bank should not dispense any money
    And the account balance should remain 100$
    And the Account Holder should be informed that they have insufficient funds
    
Scenario: Unsuccessful withdrawal due to a locked account
    Given the account is locked
    When the Account Holder requests any amount to be withdrawn
    Then the bank should not dispense any money
    And the Account Holder should be informed that their account is locked
    
    
    
    
    